<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2018/8/9 0009
  Time: 18:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <title>$Title$</title>
  </head>
  <body>
  <table>
    <th>
      <td>name</td>
      <td>level</td>
      <td>source</td>
      <td>industry</td>
      <td>address</td>
      <td>phone</td>
    </th>
    <c:forEach items="${customers}" var="customer">
    <tr>
    <td>${customer.custName}</td>
      <td>${customer.custLevel}</td>
      <td>${customer.custSource}</td>
      <td>${customer.custIndustry}</td>
      <td>${customer.custAddress}</td>
      <td>${customer.custPhone}</td>
    </tr>
    </c:forEach>
  </table>

  </body>
</html>
