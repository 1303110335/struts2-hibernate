package com.itheima.dao;

import com.itheima.domain.Customer;

import java.util.List;

/**
 * 客户的持久层方法
 */
public interface ICustomerDao {
    /**
     * 查询所有客户
     * @return
     */
    List<Customer> findAllCustomer();
}
