package com.itheima.web.action;

import com.itheima.domain.Customer;
import com.itheima.service.ICustomerService;
import com.itheima.service.impl.CustomerServiceImpl;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 客户的动作类
 */
public class CustomerAction extends ActionSupport {

    private ICustomerService customerService = new CustomerServiceImpl();
    /**
     * 查询所有客户
     * @return
     */
    public String findAllCustomer() {
        //1.调用service查询客户
        List<Customer> customers = customerService.findAllCustomer();
        //2.获取request对象
        HttpServletRequest request = ServletActionContext.getRequest();
        //3.吧查询的结果存入请求雨中
        request.setAttribute("customers", customers);
        //4.返回
        return "findAllCustomer";
    }

    public String demo1() {
        System.out.println("demo1方法执行了");
//        return "success";
//        return ERROR;
        return "login";
    }


    public String demo2() {
        System.out.println("demo2方法执行了");
        return "success";
//        return ERROR;
    }
}
